#include <SD.h>
#include <SPI.h>
// #include <MsTimer2.h>
#include <XBee.h>
/* Global variables declaration */
#include <TapirUtils.h>
#include <TapirGlobal.h>

#define DEBUG 1

const static uint8_t MAX_BYTES = 95;
const static uint8_t START_IMAGE_TX = 0x19;
const static uint8_t END_IMAGE_TX = 0x20;

uint8_t nodes_count;
XBeeAddress64 discovered_nodes[20];
XBee xbee;
// volatile int elapsed = 0;
// void increment_elapsed() {
//     elapsed++;
// }

// image_retrieval_schedule - most likely a structure that holds a specific hour, day, week

/* private functions */

/* Arduino bootstrap functions */
void setup() {
    Serial.begin(115200);
    while(! Serial) {
	;
    }
    // initialize global variables
    // set up RF board
    Serial1.begin(115200);
    while(!Serial1) {
	;
    }
    xbee.setSerial(Serial1);
    // set up SD card
    pinMode(10, OUTPUT);
    if (!SD.begin(4)) {
	Serial.println("SD initialization failed.");
	// return;
    }
    else {
	Serial.println("SD initialization done.");
    }
    // set up timers
    // set up interrupts
    // set ON Led if any of the above failed otherwise carry on

    randomSeed(analogRead(0));
    //
}

void loop() {
    // verify local storage healthy
    // verify battery life
    // compute_topology_graph
    Serial.println("ready");
    switch (blocking_read()) {
	// TODO: case statement serves a development process only. this should be removed
    case '0':
	nodes_count = run_node_discovery(discovered_nodes);
	Serial.print("Nodes found: ");
	Serial.println(nodes_count);
	for (int i = 0; i < nodes_count; i++) {
	    fetch_and_store_images(discovered_nodes[i]);
	    print_stats();
	}
	break;
    case '1':
	run_node_discovery(discovered_nodes);
    default:
	break;
    }
}

uint8_t blocking_read() {
    while (!Serial.available())
	;
    return Serial.read();
}


/* Run the FN command through the API and return number of nodes discovered */
uint8_t run_node_discovery(XBeeAddress64 nodes[]) {
    // parse response, create XBeeAddress64 from it and store it in nodes.
    uint8_t ndCommand[] = {'N', 'D'};
    AtCommandRequest atRequest = AtCommandRequest(ndCommand);
    AtCommandResponse atResponse = AtCommandResponse();
    xbee.send(atRequest);
    // NOTE: assuming we won't have a network of more than 256 nodes.
    uint8_t nodes_count = 0;

    // MsTimer2::set(1000, increment_elapsed);
    // MsTimer2::start();
    uint32_t remoteAddrMsb = 0;
    uint32_t remoteAddrLsb = 0;

    int elapsed = millis();
    while (elapsed < 20000) {
    // while (elapsed < 20) {
	// wait 1 second for the status response
	if (xbee.readPacket(1000)) {
	    // got a response!

	    // should be an AT command response
	    if (xbee.getResponse().getApiId() == AT_COMMAND_RESPONSE) {
		xbee.getResponse().getAtCommandResponse(atResponse);

		if (atResponse.isOk()) {
		    if (atResponse.getValueLength() > 10) {
			remoteAddrMsb |= (uint32_t)atResponse.getValue()[2] << 24;
			remoteAddrMsb |= (uint32_t)atResponse.getValue()[3] << 16;
			remoteAddrMsb |= (uint32_t)atResponse.getValue()[4] << 8;
			remoteAddrMsb |= (uint32_t)atResponse.getValue()[5];
			remoteAddrLsb |= (uint32_t)atResponse.getValue()[6] << 24;
			remoteAddrLsb |= (uint32_t)atResponse.getValue()[7] << 16;
			remoteAddrLsb |= (uint32_t)atResponse.getValue()[8] << 8;
			remoteAddrLsb |= (uint32_t)atResponse.getValue()[9];
			Serial.print(remoteAddrMsb, HEX);
			Serial.println(remoteAddrLsb, HEX);
			nodes[nodes_count] = XBeeAddress64(remoteAddrMsb, remoteAddrLsb);
			nodes_count++;
			// restart variables
			remoteAddrMsb = 0;
			remoteAddrLsb = 0;
		    }
		}
		else {
		    Serial.print("Command return error code: ");
		    Serial.println(atResponse.getStatus(), HEX);
		}
	    } else {
		Serial.print("Expected AT response but got ");
		Serial.println(xbee.getResponse().getApiId(), HEX);
	    }
	} else {
	    // at command failed
	    if (xbee.getResponse().isError()) {
		Serial.print("Error reading packet.  Error code: ");
		Serial.println(xbee.getResponse().getErrorCode());
	    }
	    else {
		Serial.println("No response from radio");
	    }
	}
	elapsed = millis();
    }
    // elapsed = 0;
    // MsTimer2::stop();
    return nodes_count;
}

void fetch_and_store_images(XBeeAddress64 addr64from) {
    // receive node info, like how many images will be transfered.
    // we can use this information to setup a timer and not wait indefinitely for node to transmit images.
    // receive images from node
    Serial.println("retrieving images");
    uint8_t num_images = get_node_info(addr64from);
    Serial.print("# images: ");
    Serial.println(num_images, DEC);
    if (num_images > 0) {
	get_node_images(addr64from, num_images);
    }
}

static uint8_t get_node_info(XBeeAddress64 addr64from) {
    uint8_t payload[] = {0x28};
    ZBTxRequest zbTx = ZBTxRequest(addr64from, payload, 1);
    xbee.send(zbTx);
    ZBRxResponse zbRx = ZBRxResponse();
    // not interested to read TR status response, discard it
    xbee.readPacket(1000);
    // am interested to read actual response from other node.
    xbee.readPacket(1000);
    if (xbee.getResponse().isAvailable()) {
	Serial.println("got response");
	if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
	    xbee.getResponse().getZBRxResponse(zbRx);
	    if (zbRx.getDataLength() > 0) {
		Serial.println(zbRx.getData(0));
		return zbRx.getData(0);
	    }
	    else {
		Serial.println("not enough data");
	    }
	}
	else {
	    Serial.println("expected ZB_RX response");
	}
    }
    return 0;
}

static void get_node_images(XBeeAddress64 addr64from, uint8_t number_images) {
    uint8_t payload[] = {0x30};
    ZBTxRequest zbTx = ZBTxRequest(addr64from, payload, 1);
    xbee.send(zbTx);
    xbee.readPacket(1000);
#ifdef DEBUG
    unsigned long start = millis();
#endif
    for (;number_images > 0; number_images--) {
#ifdef DEBUG
	Serial.print("Starting to receive image #");
	Serial.println(number_images);
#endif
	receive_image();
    }
#ifdef DEBUG
    Serial.print(":>Images reception completed in ");
    Serial.println(millis() - start);
#endif
#ifdef DEBUG
    Serial.print("Finished receiving images from: ");
    Serial.print(addr64from.getMsb(), HEX);
    Serial.println(addr64from.getLsb(), HEX);
#endif
}

static void receive_image() {
    ZBRxResponse zbRx;
    uint32_t total_size = 0;
    uint32_t creation_time = 0;
    uint32_t fragments = 0;
    uint8_t payload_bytes_read = 0;
    uint32_t total = 0;
    uint16_t sequence = 0;
    // I try to avoid using dynamic memory allocation as much as possible
    char filename[6];
    random_name(filename, sizeof(filename));
    File file = SD.open(filename, FILE_WRITE);

    xbee.readPacket(1000);
    if (xbee.getResponse().isAvailable()) {
	if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
	    xbee.getResponse().getZBRxResponse(zbRx);
	    if (zbRx.getData(0) == START_IMAGE_TX) {
		total_size |= (uint32_t)zbRx.getData(1) << 24;
		total_size |= (uint32_t)zbRx.getData(2) << 16;
		total_size |= (uint32_t)zbRx.getData(3) << 8;
		total_size |= (uint32_t)zbRx.getData(4);
		Serial.println(total_size);
		fragments |= (uint32_t)zbRx.getData(5) << 24;
		fragments |= (uint32_t)zbRx.getData(6) << 16;
		fragments |= (uint32_t)zbRx.getData(7) << 8;
		fragments |= (uint32_t)zbRx.getData(8);
		Serial.println(fragments);
		creation_time |= (uint32_t)zbRx.getData(9) << 24;
		creation_time |= (uint32_t)zbRx.getData(10) << 16;
		creation_time |= (uint32_t)zbRx.getData(11) << 8;
		creation_time |= (uint32_t)zbRx.getData(12);
		Serial.println(creation_time);

		while (fragments > 0) {
		    xbee.readPacket(1000);
		    if (xbee.getResponse().isAvailable()) {
			if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
			    xbee.getResponse().getZBRxResponse(zbRx);
			    payload_bytes_read = zbRx.getDataLength();
			    total += payload_bytes_read;
			    Serial.print(".");
			    sequence = receive_block(&file, zbRx.getData(), payload_bytes_read, sequence);
			    // if more than two invalid read then goto END_FILE_CLOSE
			    // goto END_FILE_CLOSE
			    --fragments;
			}
		    }
		}
#ifdef DEBUG
		Serial.print("Image reception done. ");
		Serial.print(total);
		Serial.println(" bytes received");
#endif
	    }
	}
	else {
	    Serial.println("expected a ZB_RX_RESPONSE");
	}
    }
    else {
	Serial.println("Nothing to read");
    }
 END_FILE_CLOSE:
    file.close();
}

uint16_t receive_block(File *file, uint8_t *raw_data, uint8_t data_len, uint16_t previous_sequence) {
    // first two bytes are the sequence number
    uint16_t sequence;
    sequence = ((uint16_t)raw_data[0] << 8) | (uint16_t)raw_data[1];
#if DEBUG
    unsigned long tm;
#endif
    // actual_data_len = raw_data[2];
    // advance pointer two bytes
    raw_data += 3;
    data_len -= 3;
    // are we looking at the next sequence?
    if (sequence > previous_sequence) {
    	if (sequence == previous_sequence + 1) {
#if DEBUG
	    // tm = micros();
    	    file->write(raw_data, data_len);
	    // Serial.print(micros() - tm);
#else
	    file->write(raw_data, data_len);
#endif
    	    // Serial.print("Writing block #");
    	    // Serial.println(sequence);
    	}
    	else {
    	    // FIXME: is it ok to fill-in null bytes for the lost blocks?
    	    int d = sequence - previous_sequence;
	    Serial.println("");
	    Serial.print("Sync lost: last sequence: ");
	    Serial.print(previous_sequence);
	    Serial.print(" / current sequence: ");
	    Serial.println(sequence);
    	    for (int i=0; i<d; i++) {
    		// fill_null_bytes(file, MAX_BYTES_PER_BLOCK);
    		Serial.print("^");
    	    }
    	}
    }
    else {
    	// drop it
	// increase miss-reads - this mark as invalid - two invalids reads break current loop
	// ++miss_reads;
    }
    // file->write(raw_data, data_len);
    return sequence;
}

void fill_null_bytes(File *file, int number_null_bytes) {
    uint8_t null_data[number_null_bytes];
    memset(null_data, (uint8_t) 0, number_null_bytes);
    if (file) {
	file->write(null_data, number_null_bytes);
    }
}
